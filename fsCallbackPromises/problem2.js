const fs = require('fs');

/*
1. Read the given file lipsum.txt
2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function readGivenFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, "utf-8", (err, data) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    });
}

function writeGivenFile(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(fileName, data, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    });
}

function appendGivenFile(fileName, data) {
    return new Promise((resolve, reject) => {
        fs.appendFile(fileName, data, (err) => {
            if (err) {
                reject(err)
            } else {
                resolve(data)
            }
        })
    });
}

function deleteFiles(fileName) {
    return new Promise((resolve, reject) => {
        fileName.forEach((element) => {
            fs.unlink(element, (err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve()
                }
            })
        })
    });
}


function allReturnFn(fileName) {
    readGivenFile(fileName)

        .then((data) => {
            console.log("lipsum.txt has been read.")
            const upperData = data.toUpperCase()
            return writeGivenFile("../fsCallbackPromises/upperCaseFile.txt", upperData)

        }).then(() => {
            console.log("Given file converted into uppercase file.")
            return writeGivenFile("../fsCallbackPromises/fileNames.txt", "upperCaseFile.txt")

        }).then((data) => {
            console.log("upperCaseFile name in filenames.txt file.")
            return readGivenFile(data)

        }).then((data) => {
            console.log("New uppercase file read successfully.")
            const lowerData = data.toLowerCase().replaceAll('\n', '').split('. ').join("\n")
            return writeGivenFile("../fsCallbackPromises/lowerCaseFile.txt", lowerData)

        }).then(() => {
            console.log("New file converted into lowercase file.")
            const appendFileName = "lowerCaseFile.txt"
            return appendGivenFile("../fsCallbackPromises/fileNames.txt", `\n${appendFileName}`)

        }).then(() => {
            console.log("lowerCaseFile name added to filenames.txt file.");
            return readGivenFile(`lowerCaseFile.txt`)

        }).then((data) => {
            console.log("Lower case file read successfully.");
            const sortData = data.split("\n").sort().join("\n")
            return writeGivenFile("../fsCallbackPromises/sortedContentFile.txt", sortData)

        }).then(() => {
            console.log("Sort the content of new file.")
            return appendGivenFile("../fsCallbackPromises/fileNames.txt", "\nsortedContentFile.txt")

        }).then(() => {
            console.log("sortedContentFile.txt name added successfully in fileNames.txt file.")
            return readGivenFile("../fsCallbackPromises/fileNames.txt")

        }).then((data) => {
            console.log("fileNames.txt file read successfully.")
            const fileName = data.split("\n")
            return deleteFiles(fileName)

        }).then(() => {
            console.log("All files deleted successfully.");

        }).catch((err) => {
            console.log(".cath: ", err)
        })
}

module.exports = allReturnFn;