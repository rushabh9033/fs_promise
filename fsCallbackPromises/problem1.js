/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
	        2. Delete those files simultaneously 
*/

const fs = require('fs');

function createDirectory(directoryName) {
    return new Promise((resolve, reject) => {
        fs.mkdir(directoryName, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log(`Directory is created.`)
                resolve(directoryName)
            }
        })
    });
}



function writeJsonFile(n) {
    return new Promise((resolve, reject) => {
        let completeExcutions = 0
        let fileNames = []

        for (let index = 0; index < n; index++) {

            let fileName = `./randomDirectory/randomFile${index}.json`
            fileNames.push(fileName)

            let data = [{
                firstName: "Rushabh",
                age: 29,
                lastName: "Navapara",
            }, {
                "fileNumber": index
            }];

            fs.writeFile(fileName, JSON.stringify(data), (err) => {
                if (err) {
                    reject(err)
                } else {
                    completeExcutions += 1
                    if (completeExcutions == 4) {
                        console.log("All json files created.");

                        resolve(fileNames)
                    }

                }
            })
        }
    });
}

function deleteFile(fileName) {
    return new Promise((resolve, reject) => {
        let delExecutions = 0
        fileName.forEach((element) => {
            fs.unlink(element, (err) => {
                if (err) {
                    reject(err)
                } else {
                    delExecutions += 1
                    if (delExecutions == fileName.length) {
                        resolve("All files deleted.")
                    }
                }
            })
        })
    })


}


function allReturnFn(directoryPath) {

    createDirectory(directoryPath)
        .then(() => {
            return writeJsonFile(4)
        })
        .then((data) => {
            return deleteFile(data)

        })
        .then(data => console.log(data))
        .catch((err) => console.log(".catch: ", err))
}


module.exports = allReturnFn;